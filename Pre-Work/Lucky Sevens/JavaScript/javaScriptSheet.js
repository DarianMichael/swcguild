/*  External Javascript */
function rollDice() {
  var d1 = Math.floor(Math.random() * 6) + 1;
  var d2 = Math.floor(Math.random() * 6) + 1;
  var totalRolls = 0;
  var highestValueRoll = 0;
  var diceTotal = d1 + d2;
  var startingBet = document.getElementById("startingBet").value;
  var highestValue = startingBet;
  var currentFunds = startingBet;

  while (currentFunds > 0 ) {
    var d1 = Math.floor(Math.random() * 6) + 1;
    var d2 = Math.floor(Math.random() * 6) + 1;
    if(d1 + d2 == 7 ) {
      currentFunds = +currentFunds + 4;
      if (currentFunds > highestValue) {
        highestValue = currentFunds;
        highestValueRoll = totalRolls;
      };
    }
    else {
      currentFunds = +currentFunds - 1;
    }
    var d1 = 0;
    var d2 = 0;
    totalRolls = totalRolls + 1;
  }

  document.getElementById("firstBet").value = "$"+startingBet;
  document.getElementById("totalRolls").value = totalRolls;
  document.getElementById("highestValue").value = "$"+highestValue;
  document.getElementById("highestValueRoll").value = highestValueRoll;
  document.getElementById("startingBet").value = currentFunds;
  document.getElementById("betButton").value = "Play Again";
}
